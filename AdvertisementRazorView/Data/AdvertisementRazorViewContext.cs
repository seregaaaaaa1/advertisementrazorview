﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AdvertisementRazorView.Models;

namespace AdvertisementRazorView.Data
{
    public class AdvertisementRazorViewContext : DbContext
    {
        public AdvertisementRazorViewContext (DbContextOptions<AdvertisementRazorViewContext> options)
            : base(options)
        {
        }

        public DbSet<AdvertisementRazorView.Models.AdvertisementCard> AdvertisementCard { get; set; }
    }
}
