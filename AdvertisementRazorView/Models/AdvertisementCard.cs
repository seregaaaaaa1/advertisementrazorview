﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AdvertisementRazorView.Models
{
    public class AdvertisementCard
    {
        public int ID { get; set; }

        public int Number { get; set; }
        //{
        //    get { return Number; }
        //    set
        //    {
        //        if (value < 0)
        //        {
        //            throw new ArgumentException("Номер должен быть положительным");
        //        }
        //        Number = value;
        //    }
        //}
                     
        [DataType(DataType.Date)]
        public DateTime Created { get; set; }

        public List<User> User { get; set; }

        public string AdvertisementText { get; set; }

        public int Rating { get; set; }
    }
}


