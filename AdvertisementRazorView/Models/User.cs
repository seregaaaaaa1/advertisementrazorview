﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdvertisementRazorView.Models
{
    public class User
    {
        public int Id { get; set; } // Id вместо ID

        //private readonly string _name;

        //private readonly string _lastName;

        //public User(string name, string lastName)
        //{
        //    _name = name;
        //    _lastName = lastName;
        //}

        //public string Name => $"{_name} {_lastName}";

        public string Name { get; set; }

        public string LastName { get; set; }

    }
}
