﻿using AdvertisementRazorView.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace AdvertisementRazorView.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new AdvertisementRazorViewContext(
                serviceProvider.GetRequiredService<
                 DbContextOptions<AdvertisementRazorViewContext>>()))
            {
                if (context.AdvertisementCard.Any())
                {
                    return;
                }

                context.AdvertisementCard.AddRange(

                    new AdvertisementCard
                    {
                        Number = 1234,
                        Created = DateTime.Parse("2000-10-10"),
                        AdvertisementText = "Добавление из конструктора",
                        Rating = 256
                    }
                    );
                context.SaveChanges();
            }                   
        }
    }
}
