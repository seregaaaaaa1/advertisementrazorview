﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using AdvertisementRazorView.Data;
using AdvertisementRazorView.Models;

namespace AdvertisementRazorView.Pages.Advertisements
{
    public class CreateModel : PageModel
    {
        private readonly AdvertisementRazorView.Data.AdvertisementRazorViewContext _context;

        public CreateModel(AdvertisementRazorView.Data.AdvertisementRazorViewContext context)
        {
            _context = context;
        }

        //Метод OnGet инициализируют все состояния, необходимые для страницы
        public IActionResult OnGet()
        {
            return Page(); 
            //Возвращается Page, т.к. Create не содержит никаких состояний для инициализации
            //метод Page создаёт объект PageResult, который формирует страницу Create.cshtml
        }
        //атрибут используется для указания согласия на привязку модели в св-ве AdvertisementCard
        //когда Create публикует свои значения, среда выполнения ASP.NET Core связывает переданные значения с моделью AdvertisementCard
        [BindProperty]
        public AdvertisementCard AdvertisementCard { get; set; }

        //Так как возвращается Task<IActionResult> необходимо использовать return
        //Метод OnPostAsync выполняется когда страница публикует данные формы
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.AdvertisementCard.Add(AdvertisementCard);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}