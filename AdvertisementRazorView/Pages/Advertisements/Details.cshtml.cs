﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using AdvertisementRazorView.Data;
using AdvertisementRazorView.Models;

namespace AdvertisementRazorView.Pages.Advertisements
{
    public class DetailsModel : PageModel
    {
        private readonly AdvertisementRazorView.Data.AdvertisementRazorViewContext _context;

        public DetailsModel(AdvertisementRazorView.Data.AdvertisementRazorViewContext context)
        {
            _context = context;
        }

        public AdvertisementCard AdvertisementCard { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            AdvertisementCard = await _context.AdvertisementCard.FirstOrDefaultAsync(m => m.ID == id);

            if (AdvertisementCard == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
