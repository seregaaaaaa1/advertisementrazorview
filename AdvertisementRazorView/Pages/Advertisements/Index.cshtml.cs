﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using AdvertisementRazorView.Data;
using AdvertisementRazorView.Models;

namespace AdvertisementRazorView.Pages.Advertisements
{
    public class IndexModel : PageModel
    {
        private readonly AdvertisementRazorView.Data.AdvertisementRazorViewContext _context;

        public IndexModel(AdvertisementRazorView.Data.AdvertisementRazorViewContext context)
        {
            _context = context;    //добавляет на страницу AdvertisementRazorViewContext
        }

        public IList<AdvertisementCard> AdvertisementCard { get;set; }

        /*при запросе на страницу метод OnGetAsync возвращает на страницу список объявлений
         на странице Razor вызывается метод OnGetAsync или OnGet, инициализирующий состояние страницы
         в данном случае отоброажается список объявлений
         return не используется т.к. возвращается Task
         */
        public async Task OnGetAsync()  
        {
            AdvertisementCard = await _context.AdvertisementCard.ToListAsync();
        }
    }
}
