﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AdvertisementRazorView.Data;
using AdvertisementRazorView.Models;

namespace AdvertisementRazorView.Pages.Advertisements
{
    public class EditModel : PageModel
    {
        private readonly AdvertisementRazorView.Data.AdvertisementRazorViewContext _context;

        public EditModel(AdvertisementRazorView.Data.AdvertisementRazorViewContext context)
        {
            _context = context;
        }

        [BindProperty]
        public AdvertisementCard AdvertisementCard { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            AdvertisementCard = await _context.AdvertisementCard.FirstOrDefaultAsync(m => m.ID == id);

            if (AdvertisementCard == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(AdvertisementCard).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AdvertisementCardExists(AdvertisementCard.ID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool AdvertisementCardExists(int id)
        {
            return _context.AdvertisementCard.Any(e => e.ID == id);
        }
    }
}
