﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using AdvertisementRazorView.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AdvertisementRazorView
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;

                try
                {
                    SeedData.Initialize(services);
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred seeding the DB.");
                }
            }

            host.Run();

        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
     Host.CreateDefaultBuilder(args)
         .ConfigureWebHostDefaults(webBuilder =>
         {
             webBuilder.UseStartup<Startup>();
         });

        //коммент1111
        //readonly User user1 = new User("Tim", "Burton");

        //readonly User user2 = new User("Michael", "Jordan");

        //readonly User user3 = new User("Hanna", "Montana");

        //List<string> UsersList = new List<string>();

        //public List<string> GetUsersList()
        //{
        //    for (int i = 0; i < 2; i++)
        //    {
        //       // var tempName = $"user + {i + 1}";
        //       // UsersList.Add(tempName.Name);
        //    }

        //    return null;
        //}
        ////Список 
    }
}
